/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

/**
 *
 * @author AlexandreLRe
 */
public class Data {
    
    private int valeur;

    public Data(int valeur) {
        this.valeur = valeur;
    }
    
    /**
     * Get the value of valeur
     *
     * @return the value of valeur
     */
    public int getSize() {
        return valeur;
    }

    /**
     * Set the value of valeur
     *
     * @param size new value of valeur
     */
    public void setSize(int size) {
        this.valeur = size;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.valeur;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Data other = (Data) obj;
        if (this.valeur != other.valeur) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Data{" + "valeur=" + valeur + '}';
    }

}
