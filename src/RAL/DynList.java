/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

import java.util.Arrays;

/**
 *
 * @author AlexandreLRe
 */
public class DynList implements List{

    private Data[] values;

    public DynList() {
        this.values = new Data[0];
    }

    public DynList(DynList arrayListRef) {
        this.values = new Data[arrayListRef.values.length];
        for (int i = 0; i < arrayListRef.values.length; i++) {
            this.values[i] = arrayListRef.getValues(i);
        }
    }
    
    

    /**
     * Get the value of values
     *
     * @return the value of values
     */
    @Override
    public Data[] getValues() {
        return values;
    }
    
    public void setValues(Data[] values) {
        this.values = values;
    }
    
    public Data getValues(int index) {
        return this.values[index];
    }

    
    public void setValues(int index, Data values) {
        this.values[index] = values;
    }

    @Override
    public boolean isEmpty() {
        return this.values.length == 0;
    }

    @Override
    public void add(Data pdata) {
        //Tableau Temporaire
        Data[] temp_values = new Data[this.values.length+1];
        
        //Copie manuelle
        for (int i = 0; i < this.values.length; i++) {
            temp_values[i] = this.values[i];
        }
                
        //Ajout de valeur À la fin
        temp_values[this.values.length] = pdata;
        
        //Copie de la reference
        this.values = temp_values;
    }

    @Override
    public void remove() {
        //tableau temporaire
        Data[] temp_values = new Data[this.values.length-1];
        
        //grand tableau copie dans petit tableau; sauf derniere case
        for (int i = 0; i < temp_values.length; i++) {
            temp_values[i] = this.values[i];
        }
        
        //copie de la copie vers l'original
        this.values = temp_values;
    }

    @Override
    public void remove(int index) {
        if (index > this.values.length-1) {
            throw new RuntimeException("Index dépasse les paramètres acceptés");
        }
        
        for (int i = index; i < this.values.length; i++) {
            values[i] = values[i + 1];
        }
        
        this.remove();
    }

    @Override
    public void add(int index, Data pdata) {
        Data[] temp_values = new Data[this.values.length+1];
        
        //recopier jusqu'à  l'index
        for (int i = 0; i < index; i++) {
            temp_values[i] = this.values[i];
        }        

        temp_values[index] = pdata;
        
        for (int i = index+1; i < temp_values.length; i++) {
            temp_values[i] = this.values[i-1];
        }
        
        this.values = temp_values;
    }

    @Override
    public Data getLast() {
        return this.values[this.values.length-1];
    }

    @Override
    public Data getFirst() {
        return this.values[0];
    }
    
    @Override
    public Data get(int index) {
        if(this.values[index] == null)
        {
            throw new RuntimeException("Cette case est vide!");
        }
        return this.values[index];
    }
    
    @Override
    public void clear()
    {
        this.values = new Data[0];
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Arrays.deepHashCode(this.values);
        return hash;
    }

    @Override
    public String toString() {
        return "DynList{" + "values=" + values + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DynList other = (DynList) obj;
        if (!Arrays.deepEquals(this.values, other.values)) {
            return false;
        }
        return true;
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
