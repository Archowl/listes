/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

/**
 *
 * @author AlexandreLRe
 */
public class Node {
    Node next;
    Data value;
    
    public Node(Data value,Node next) {
        this.next = next;
        this.value = value;
    }
    
    public Data getValeur()
    {
        return this.value;
    }
    
    public void setValeur(Data valeur)
    {
        this.value = valeur;
    }
    
    public Node getNext()
    {
        return this.next;
    }
    
    public void setNext(Node node)
    {
        this.next = node;
    }
}
