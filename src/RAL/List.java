/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

/**
 *
 * @author AlexandreLRe
 */
public interface List {

    void add(Data pdata);

    void add(int index, Data pdata);

    void clear();

    Data get(int index);

    Data getFirst();

    Data getLast();
    
    Data[] getValues();

    boolean isEmpty();

    void remove();

    void remove(int index);
    
    int size();
    
}
