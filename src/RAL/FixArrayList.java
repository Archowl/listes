/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

import java.util.Arrays;

/**
 *
 * @author AlexandreLRe
 */
public class FixArrayList implements List{

    private int size = 0;
    private Data[] values;

    public FixArrayList(int pSize) {
        if (pSize < 0) {
            throw new RuntimeException("Valeur saisi n'est pas positif");
        }
        this.values = new Data[pSize];
    }

    public FixArrayList(FixArrayList arrayListRef) {
        this.values = new Data[arrayListRef.values.length];
        for (int i = 0; i < arrayListRef.size(); i++) {
            this.values[i] = arrayListRef.getValues(i);
            this.size++;
        }
    }
    
    

    /**
     * Get the value of values
     *
     * @return the value of values
     */
    public Data[] getValues() {
        return values;
    }

    /**
     * Set the value of values
     *
     * @param values new value of values
     */
    public void setValues(Data[] values) {
        this.values = values;
    }

    /**
     * Get the value of values at specified index
     *
     * @param index the index of values
     * @return the value of values at specified index
     */
    public Data getValues(int index) {
        return this.values[index];
    }

    /**
     * Set the value of values at specified index.
     *
     * @param index the index of values
     * @param values new value of values at specified index
     */
    public void setValues(int index, Data values) {
        this.values[index] = values;
    }


    /**
     * Set the value of size
     *
     * @param size new value of size
     */
    public void setSize(int size) {
        this.size = size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public void add(Data data11) {
        this.values[this.size] = data11;
        this.size++;
    }

    public int size() {
        return this.size;
    }

    public void remove() {
        this.size--;
    }

    public void remove(int index) {
        if (this.values[index] == null) {
            throw new RuntimeException("La case est vide");
        }
        for (int i = index; i < this.size; i++) {
            values[i] = values[i + 1];
        }
        this.size--;
    }

    public void add(int index, Data pdata) {
        if (this.size == this.values.length) {
            throw new RuntimeException("Le tableau est déjà plein");
        }

        for (int j = this.size; j > index; j--) {
            this.values[j] = this.values[j - 1];
        }

        this.values[index] = pdata;
        this.size++;
    }

    public Data getLast() {
        return this.values[this.size-1];
    }

    public Data getFirst() {
        return this.values[0];
    }
    
    public Data get(int index) {
        if(this.values[index] == null)
        {
            throw new RuntimeException("Cette case est vide!");
        }
        return this.values[index];
    }
    
    public void clear()
    {
        this.size = 0;
    }

    @Override
    public String toString() {
        return "FixArrayList{" + "size=" + size + ", values=" + values + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.size;
        hash = 41 * hash + Arrays.deepHashCode(this.values);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FixArrayList other = (FixArrayList) obj;
        if (this.size != other.size) {
            return false;
        }
        if (!Arrays.deepEquals(this.values, other.values)) {
            return false;
        }
        return true;
    }
    
}
