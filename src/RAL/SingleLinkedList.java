/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

/**
 *
 * @author AlexandreLRe
 */
public class SingleLinkedList implements List {

    private Node first;

    public SingleLinkedList() {
    }

    public SingleLinkedList(SingleLinkedList list) {
        if (!list.isEmpty()) {
            Node node = new Node(list.getFirst(), null);
            this.first = node;
            
            for (int i = 1; i < list.size(); i += 1) 
            {
                Node next = new Node(list.get(i), null);
                node.setNext(next);
                node = next;
            }
        }

    }

    @Override
    public void add(Data pdata) {
        Node _Node = new Node(pdata, null);
        if (this.first == null) {
            this.first = _Node;
        } else {
            //si ça contient >1 element
            Node lastNode = this.first;
            while (lastNode.getNext() != null) {
                lastNode = lastNode.getNext();
            }
            lastNode.setNext(_Node);
        }
    }

    @Override
    public void add(int index, Data pdata) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        this.first = null;
    }

    @Override
    public Data get(int index) {
        int icpt = 0;
        Node lastNode = this.first;
        if (index == icpt) {
            return lastNode.value;
        }
        while (lastNode.getNext() != null) {
            lastNode = lastNode.getNext();
            icpt++;
            if (index == icpt) {
                return lastNode.value;
            }
        }
        return null;
    }

    @Override
    public Data getLast() {
        Node lastNode = this.first;
        while (lastNode.getNext() != null) {
            lastNode = lastNode.getNext();
        }
        return lastNode.getValeur();
    }

    @Override
    public Data[] getValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        return this.first == null;
    }

    @Override
    public void remove() {
        Node lastNode = this.first;
        Node previoustolastNode = this.first;
        while (lastNode.getNext() != null) {
            if (lastNode != this.first) {
                previoustolastNode = lastNode;
            }
            lastNode = lastNode.getNext();
        }
        previoustolastNode.next = null;
    }

    @Override
    public void remove(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Data getFirst() {
        return this.first.value;
    }

    @Override
    public int size() {
        if (this.isEmpty()) {
            return 0;
        }
        int icpt = 0;
        Node lastNode = this.first;
        if (this.first != null) {
            icpt++;
        }
        while (lastNode.getNext() != null) {
            lastNode = lastNode.getNext();
            icpt++;
        }
        return icpt;
    }

}
