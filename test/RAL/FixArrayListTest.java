/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RAL;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AlexandreLRe
 */
public class FixArrayListTest {
    FixArrayList FAL1;
    Data data11, data22, data33, data44;
    
    public FixArrayListTest() {
    }
    
    @Before
    public void setUp() {
        FAL1 = new FixArrayList(3);
        data11 = new Data(11);
        data22 = new Data(22);
        data33 = new Data(33);
        data44 = new Data(44);
    }

    @Test
    public void testAjouterRemoveSansIndice() {
        //Tableau vide
        assertEquals(true, FAL1.isEmpty());
        
        //Ajout de donnée
        FAL1.add(data11);
        assertEquals(false, FAL1.isEmpty());
        assertEquals(1, FAL1.size());
        
        //Supprimer une donnée
        FAL1.remove();
        assertEquals(true, FAL1.isEmpty());
        
        
    }
    
    @Test
    public void testAjouterRemoveAvecIndice()
    {
        //Tableau vide
        assertEquals(true, FAL1.isEmpty());
                
        //ajout indexé
        FAL1.add(0, data33);
        assertEquals(false, FAL1.isEmpty());
        FAL1.remove();
        assertEquals(true, FAL1.isEmpty());
        
        FAL1.add(data11);
        FAL1.remove(0);
        assertEquals(true, FAL1.isEmpty());
    }
    
    @Test
    public void testFirstEtLast()
    {
        FAL1.add(data11);
        FAL1.add(data22);
        FAL1.add(data33);
        
        assertEquals(data33, FAL1.getLast());
        
        assertEquals(data11, FAL1.getFirst());
    }
    
    @Test
    public void testGet(){        
        FAL1.add(data11);
        FAL1.add(data22);
        FAL1.add(data33);
        assertEquals(data22, FAL1.get(1));
    }
    
    @Test
    public void testEmpty(){
        assertEquals(true, FAL1.isEmpty());
    }
    
}
