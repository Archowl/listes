package RAL;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dominique huguenin (dominique.huguenin at rpn.ch)
 */
public class DynListTestDHU1 {
    private DynList arrayListRef;
    private Data[] elementsRef;

    public DynListTestDHU1() {
    }

    @Before
    public void setUp() {
        arrayListRef = new DynList();
        elementsRef = new Data[]{new Data(10),
            new Data(20),
            new Data(30)};
    }

    /**
     * Test of size method, of class FixArrayList.
     */
    @Test
    public void testSize() {
    }

    @Test
    public void testCreationListe() throws Exception {
        Assert.assertEquals("Erreur:La Liste devrait contenir 0 élément!",
                true, arrayListRef.isEmpty());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testMauvaiseCreationListe() throws Exception {
        FixArrayList arrayList = new FixArrayList(-1);
        Assert.fail("Erreur:La création devrait générer une exception!");
    }

    @Test
    public void testAdd() throws Exception {

        Data element = new Data(10);
        arrayListRef.add(element);

        Assert.assertEquals("Erreur:La Liste devrait contenir 1 élément!",
                element, arrayListRef.get(0));

        Assert.assertEquals(element, arrayListRef.getLast());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddListPlein() throws Exception {
        FixArrayList arrayList = new FixArrayList(0);

        arrayList.add(elementsRef[1]);
        Assert.fail("Erreur:La Liste devrait être plein!");
    }

    @Test
    public void testAddEnPosition() throws Exception {

        arrayListRef.add(elementsRef[0]);
        arrayListRef.add(elementsRef[1]);

        int position = 1;
        arrayListRef.add(position, elementsRef[2]);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[2], arrayListRef.get(position));
        Assert.assertEquals(elementsRef[1], arrayListRef.get(position + 1));

    }

    @Test
    public void testAddEnPosition2() throws Exception {
        arrayListRef.add(0, elementsRef[0]);
        arrayListRef.add(1, elementsRef[1]);

        int position = 2;
        arrayListRef.add(position, elementsRef[2]);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[2], arrayListRef.get(position));

    }

    @Test
    public void testRemove() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        int size = arrayListRef.getValues().length;

        arrayListRef.remove();

        Assert.assertEquals(
                size - 1, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[1], arrayListRef.getLast());

    }

    @Test
    public void testRemoveEnPosition() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        int size = arrayListRef.getValues().length;

        int position = 1;
        arrayListRef.remove(position);

        Assert.assertEquals(
                size - 1, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[2], arrayListRef.get(position));
    }

    @Test
    public void testGetFirst() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[0], arrayListRef.getFirst());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetFirstListVide() throws Exception {
        arrayListRef.getFirst();
        Assert.fail("Erreur:La Liste devrait être vide!");
    }

    @Test
    public void testGetLast() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, arrayListRef.getValues().length);

        Assert.assertEquals(elementsRef[2], arrayListRef.getLast());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetLastListVide() throws Exception {
        arrayListRef.getLast();
        Assert.fail("Erreur: Liste devrait retourner une erreur de liste vide");
    }

    @Test
    public void testGet() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, arrayListRef.getValues().length);

        int position = 1;
        Assert.assertEquals(elementsRef[position], arrayListRef.get(position));

    }

    /*@Ignore
    @Test
    public void testToString() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        String str = "Data{valeur=10} Data{valeur=20} Data{valeur=30} ";

        Assert.assertEquals(str, arrayListRef.toString());

    }*/

    @Test
    public void testClear() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 éléments!",
                3, arrayListRef.getValues().length);

        arrayListRef.clear();

        Assert.assertEquals("Erreur:La Liste devrait contenir 0 élément!",
                0, arrayListRef.getValues().length);

    }

    @Test
    public void testClone() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        DynList clone = new DynList(arrayListRef);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 éléments!",
                3, clone.getValues().length);

        Assert.assertNotSame(elementsRef, clone);
        Assert.assertEquals(arrayListRef.getValues().length, clone.getValues().length);

        for (int i = 0; i < clone.getValues().length; i++) {
            Assert.assertEquals(arrayListRef.get(i), clone.get(i));
            Assert.assertSame(arrayListRef.get(i), clone.get(i));
            
        }
    }
    
    @Test
    public void testClone2() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        DynList clone = new DynList(arrayListRef);

        
        Data data_111 = new Data(111);
        Data data_222 = new Data(222);        
        
        arrayListRef.add(data_111);
        clone.add(data_222);
        
        Assert.assertNotEquals(data_222, arrayListRef.getLast());
        Assert.assertEquals(data_111, arrayListRef.getLast());
        Assert.assertEquals(data_222, clone.getLast());

    }
    
    @Ignore
    public void testEquals() throws Exception {
        FixArrayList arrayList = new FixArrayList(5);

        for (int i = 0; i < 3; i++) {
            arrayList.add(elementsRef[i]);
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertEquals(arrayListRef, arrayList);
    }

    @Test
    public void testIsEmpty() throws Exception {
        Assert.assertTrue("Erreur:La liste n'est pas le vide!",
                arrayListRef.isEmpty());
    }

    @Test
    public void testNotIsEmpty() throws Exception {
        for (int i = 0; i < 3; i++) {
            arrayListRef.add(elementsRef[i]);
        }

        Assert.assertFalse("Erreur:La liste est le vide!",
                arrayListRef.isEmpty());
    }

}
