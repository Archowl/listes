package RAL;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dominique huguenin (dominique.huguenin at rpn.ch)
 */
public class SingleLinkedListTest {

    
    private int tailleRef;
    private SingleLinkedList listRef;
    private Data[] elementsRef;

    public SingleLinkedListTest() {
    }

    @Before
    public void setUp() {
        tailleRef = 5;
        listRef = new SingleLinkedList();
        elementsRef = new Data[]{new Data(10),
            new Data(20),
            new Data(30)};
    }

    @Test
    public void testCreationListe() throws Exception {
        Assert.assertEquals("Erreur:La Liste devrait contenir 0 élément!",
                0, listRef.size());
    }

      @Test
    public void testAdd() throws Exception {

        Data element = new Data(10);
        listRef.add(element);

        Assert.assertEquals("Erreur:La Liste devrait contenir 1 élément!",
                1, listRef.size());

        Assert.assertEquals(element, listRef.getLast());

    }

    @Test
    public void testAddEnPosition() throws Exception {

        listRef.add(elementsRef[0]);
        listRef.add(elementsRef[1]);

        int position = 1;
        listRef.add(position, elementsRef[2]);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, listRef.size());

        Assert.assertEquals(elementsRef[2], listRef.get(position));
        Assert.assertEquals(elementsRef[1], listRef.get(position + 1));

    }

    @Test
    public void testAddEnPosition2() throws Exception {
        listRef.add(0, elementsRef[0]);
        listRef.add(1, elementsRef[1]);

        int position = 2;
        listRef.add(position, elementsRef[2]);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, listRef.size());

        Assert.assertEquals(elementsRef[2], listRef.get(position));

    }

    @Test
    public void testRemove() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        int size = listRef.size();

        listRef.remove();

        Assert.assertEquals(
                size - 1, listRef.size());

        Assert.assertEquals(elementsRef[1], listRef.getLast());

    }

    @Test
    public void testRemoveEnPosition() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        int size = listRef.size();

        int position = 1;
        listRef.remove(position);

        Assert.assertEquals(
                size - 1, listRef.size());

        Assert.assertEquals(elementsRef[2], listRef.get(position));
    }

    @Test
    public void testGetFirst() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, listRef.size());

        Assert.assertEquals(elementsRef[0], listRef.getFirst());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetFirstListVide() throws Exception {
        listRef.getFirst();
        Assert.fail("Erreur:La Liste devrait être vide!");
    }

    @Test
    public void testGetLast() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, listRef.size());

        Assert.assertEquals(elementsRef[2], listRef.getLast());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetLastListVide() throws Exception {
        listRef.getLast();
        Assert.fail("Erreur: Liste devrait retourner une erreur de liste vide");
    }

    @Test
    public void testGet() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 élément!",
                3, listRef.size());

        int position = 1;
        Assert.assertEquals(elementsRef[position], listRef.get(position));

    }

    @Test
    public void testToString() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        String str = "Data{valeur=10} Data{valeur=20} Data{valeur=30} ";

        Assert.assertEquals(str, listRef.toString());

    }

    @Test
    public void testClear() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 éléments!",
                3, listRef.size());

        listRef.clear();

        Assert.assertEquals("Erreur:La Liste devrait contenir 0 élément!",
                0, listRef.size());

    }

    @Test
    public void testClone() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        SingleLinkedList clone = new SingleLinkedList(listRef);

        Assert.assertEquals("Erreur:La Liste devrait contenir 3 éléments!",
                3, clone.size());

        Assert.assertNotSame(elementsRef, clone);
        Assert.assertEquals(listRef.size(), clone.size());

        for (int i = 0; i < clone.size(); i++) {
            Assert.assertEquals(listRef.get(i), clone.get(i));
            Assert.assertSame(listRef.get(i), clone.get(i));
            
        }
    }
    
    @Test
    public void testClone2() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        SingleLinkedList clone = new SingleLinkedList(listRef);

        
        Data data_111 = new Data(111);
        Data data_222 = new Data(222);        
        
        listRef.add(data_111);
        clone.add(data_222);
        
        Assert.assertNotEquals(data_222, listRef.getLast());
        Assert.assertEquals(data_111, listRef.getLast());
        Assert.assertEquals(data_222, clone.getLast());

    }
    
    
    @Test
    public void testEquals() throws Exception {
        FixArrayList arrayList = new FixArrayList(5);

        for (int i = 0; i < 3; i++) {
            arrayList.add(elementsRef[i]);
            listRef.add(elementsRef[i]);
        }

        Assert.assertEquals(listRef, arrayList);
    }

    @Test
    public void testIsEmpty() throws Exception {
        Assert.assertTrue("Erreur:La liste n'est pas le vide!",
                listRef.isEmpty());
    }

    @Test
    public void testNotIsEmpty() throws Exception {
        for (int i = 0; i < 3; i++) {
            listRef.add(elementsRef[i]);
        }

        Assert.assertFalse("Erreur:La liste est le vide!",
                listRef.isEmpty());
    }

}
